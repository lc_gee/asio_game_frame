# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/unit/main.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/unit/classes/CMakeFiles/test_statement.dir/__/main.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/unit/unit_fixture.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/unit/classes/CMakeFiles/test_statement.dir/__/unit_fixture.cpp.o"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/test/unit/classes/statement.cpp" "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/unit/classes/CMakeFiles/test_statement.dir/statement.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DBUG_OFF"
  "DYNLOAD_MYSQL_LIB=\"/usr/lib/x86_64-linux-gnu/libmysqlclient_r.so\""
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/driver/CMakeFiles/mysqlcppconn.dir/DependInfo.cmake"
  "/root/workspace/asio_game_frame/libs/mysql-connector-c++-1.1.5/build/test/framework/CMakeFiles/test_framework.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/mysql"
  "."
  "cppconn"
  "driver/nativeapi"
  ".."
  "../cppconn"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
