#ifndef __MODULES_LOGIN_LOGIN_MODULE_H__
#define __MODULES_LOGIN_LOGIN_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/module.h>

class AccountMgr;
class Player;
class Session;

class LoginModule : public IModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                            const std::shared_ptr<Player>& player,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

private:
    bool onLoginReq(const std::shared_ptr<Session>& session,
                    const MessageID& msgID,
                    const char* buf,
                    std::size_t bufSize);

    bool onCreatePlayerReq(const std::shared_ptr<Session>& session,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize);

    bool onChoosePlayerReq(const std::shared_ptr<Session>& session,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize);

    bool onLoginConfirm(const std::shared_ptr<Session>& session,
                        const MessageID& msgID,
                        const char* buf,
                        std::size_t bufSize);

private:
    bool retPlayerList(const std::shared_ptr<Session>& session,
                       const std::shared_ptr<AccountMgr>& accMgr,
                       uint32_t accID);

};

#endif // __MODULES_LOGIN_LOGIN_MODULE_H__
