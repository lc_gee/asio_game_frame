#include "common_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <common/log/log.h>
#include <common/net/message_parse.h>
#include <common/net/session.h>

#include <protocols/protos/common.pb.h>
using namespace proto::common;
#include <protocols/protos/db.pb.h>
#include <protocols/protos/common.pb.h>
using namespace proto::common;
#include <protocols/protos/tables.pb.h>
using namespace proto::tables;

#include <common/utils/util.h>

#include "base/account_mgr.h"
#include "base/player.h"
#include "base/player_mgr.h"
#include "base/sub_server_mgr.h"
#include "common.pb.h"
#include "net/custom_data.h"

bool CommonModule::doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                             const std::shared_ptr<Player>& player,
                             const MessageID& msgID,
                             const char* buf,
                             std::size_t bufSize)
{
    LOG_WARN("doDispatch not implement.");
    return false;
}

bool CommonModule::doDispatch(const std::shared_ptr<Session>& session,
                             const MessageID& msgID,
                             const char* buf,
                             std::size_t bufSize)
{
    switch (msgID.stMsg.msgID)
    {
    // case proto::common::C2S_COMMON_GATE_REQ:
    //     return onCommonGateReq(session, msgID, buf, bufSize);

    case proto::common::S2C_COMMON_GATE_RET:
        return onCommonGateRet(session, msgID, buf, bufSize);

    default:
        return false;
    }

    return false;
}

bool CommonModule::doTranslate(const std::shared_ptr<Session>& session,
                               const MessageID& msgID,
                               const char* buf,
                               std::size_t bufSize)
{
    const auto& cltCustom = getCustom<CltSessionCustomData>(session);
    const auto& player = cltCustom.getPlayer();
    if (!player)
    {
        LOG_ERROR("No player was found, when trans msg, msg id:" << std::hex << msgID.msgID);
        return false;
    }
    const auto& gsSession = player->getGsSession();
    if (!gsSession)
    {
        LOG_ERROR("clt session no gs session, when trans msg, msg id:" << std::hex << msgID.msgID);
        return false;
    }

    CommonGateReq req;
    req.set_player_id(player->getID());
    req.set_msg_id(msgID.msgID);
    req.set_msg(std::move(std::string(buf, bufSize)));
    req.set_msg_len(bufSize);

    gsSession->sendMsg(eCommon, proto::common::C2S_COMMON_GATE_REQ, req);
    return true;
}

bool CommonModule::onCommonGateRet(const std::shared_ptr<Session>& session,
                                  const MessageID& msgID,
                                  const char* buf,
                                  std::size_t bufSize)
{
    CommonGateRet ret;
    if (!ret.ParseFromArray(buf, bufSize))
    {
        LOG_ERROR("Parse proto data failed.");
        return false;
    }
    const auto&  playerMgr = ServerMgr::get().getSubServ<SubServerMgr>()->getSubMgr<PlayerMgr>(ePlayer);
    if (!playerMgr)
    {
        LOG_ERROR("Player manager not found.");
        return false;
    }
    const auto& player = playerMgr->getPlayer(ret.player_id());
    if (!player)
    {
        LOG_ERROR("Player not found, id:" << ret.player_id());
        return false;
    }
    const auto& cltSession = player->getCltSession();
    if (!cltSession)
    {
        LOG_ERROR("Player Client session not set, player id:" << ret.player_id());
        return false;
    }
    cltSession->sendMsg(MessageID(ret.msg_id()), ret.msg().c_str(), ret.msg().size());
    return true;
}
