#ifndef __GAME_SERVER_BASE_PLAYER_MGR_H__
#define __GAME_SERVER_BASE_PLAYER_MGR_H__

#include <map>
#include <mutex>

#include <common/base/manager.h>

class Player;

class PlayerMgr : public IManager
{
public:
    virtual bool init();

    std::shared_ptr<Player> getPlayer(uint32_t playerID)
    {
        std::lock_guard<std::mutex> lock(mutex_);
        auto it = players_.find(playerID);
        return it == players_.end() ? std::shared_ptr<Player>() : it->second;
    }

    void addPlayer(const std::shared_ptr<Player>& player);

private:
    std::mutex mutex_;
    std::map<uint32_t, std::shared_ptr<Player>> players_;
};

#endif // __GAME_SERVER_BASE_PLAYER_MGR_H__
