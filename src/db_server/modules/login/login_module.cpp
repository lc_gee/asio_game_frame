#include "login_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <common/log/log.h>
#include <common/net/message_parse.h>
#include <common/net/session.h>

#include <protocols/protos/common.pb.h>
using namespace proto::common;
#include <protocols/protos/login.pb.h>
using namespace proto::login;

#include "base/player.h"

bool LoginModule::doDispatch(const std::shared_ptr<Player>& player,
                             const MessageID& msgID,
                             const char* buf,
                             std::size_t bufSize)
{
    switch (msgID.stMsg.msgID)
    {
    case C2S_LOGIN_REQ:
        return onLogin(player, msgID, buf, bufSize);

    default:
        return false;
    }

    return false;
}

bool LoginModule::onLogin(const std::shared_ptr<Player>& player,
                          const MessageID& msgID,
                          const char* buf,
                          std::size_t bufSize)
{
    LoginReq req;
    if (!req.ParseFromArray(buf, bufSize))
    {
        LOG_ERROR("Parse proto data failed.");
        return false;
    }
    LOG_DEBUG("Got login req, acc_name: " << req.acc_name());

    LoginRet ret;
    ret.set_err(eSUC);
    player->sendMsg(eLogin, S2C_LOGIN_RET, ret);

    return true;
}
