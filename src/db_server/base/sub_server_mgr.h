#ifndef __SUB_SERVER_MGR_H__
#define __SUB_SERVER_MGR_H__

#include <common/base/sub_server_mgr.h>

enum SubManagerType : uint16_t
{
    eDB,
    eAccount,
    ePlayer,
};

enum ConfigType : uint16_t
{ // 不要定义为0，0已经被config.xml占用
    eSubServer = 1,
};

enum SessionType : uint16_t;

/**
 * @desc 客户端管理器
 * @auth Qiwei.Gu
 * @date 2015-03-25 23:29:57
 */
class SubServerMgr : public ISubServerMgr
{
public:
    /**
     * @desc 构造函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:12
     */
    SubServerMgr();

    /**
     * @desc 析构函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:36
     */
    virtual ~SubServerMgr() {}

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-03-25 23:30:54
     */
    virtual bool init();

    /**
     * @desc 添加管理器类
     * @auth Qiwei.Gu
     * @date 2015-03-21 12:26:30
     */
    bool addSubMgr(SubManagerType type, std::shared_ptr<IManager> mgr);

    /**
     * @desc 查询管理器类
     * @auth Qiwei.Gu
     * @date 2015-03-21 12:57:13
     */
    std::shared_ptr<IManager> getSubMgr(SubManagerType type);

    /**
     * @desc 查询管理器模版类
     * @auth Qiwei.Gu
     * @date 2015-03-21 12:57:13
     */
     template<typename _T>
     std::shared_ptr<_T> getSubMgr(SubManagerType type)
     {
         auto mgr = getSubMgr(type);
         return mgr ? std::dynamic_pointer_cast<_T>(mgr) : std::shared_ptr<_T>();
     }

private:
    /**
     * @desc 功能模块初始化
     * @auth Qiwei.Gu
     * @date 2015-04-05 22:04:58
     */
    bool initModule();

    /**
     * @desc 命令模块出现化
     * @auth Qiwei.Gu
     * @date 2015-04-05 22:05:42
     */
    bool initCmdModule();

    /**
     * @desc 初始化配置
     * @auth Qiwei.Gu
     * @date 2015-04-15 15:14:10
     */
    bool initConfig();

    /**
     * @desc 初始化管理器
     * @auth Qiwei.Gu
     * @date 2015-04-15 17:19:12
     */
    bool initSubMgr();

private:
    std::map<SubManagerType, std::shared_ptr<IManager>> subMgrs_;         // 各种管理器
};

#endif // __SUB_SERVER_MGR_H__
