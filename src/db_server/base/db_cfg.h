#ifndef __BASE_DB_CFG_H__
#define __BASE_DB_CFG_H__

#include <map>
#include <string>

/**
 * @desc 连接配置
 * @auth Qiwei.Gu
 * @date 2015-04-15 14:49:24
 */
struct DBConfigInfo
{
    std::string addr;   // 数据库地址
    std::string port;   // 数据库端口
    std::string user;   // 用户名
    std::string passwd; // 密码
    std::string dbname; // 库名

    std::string sqlSchema; // 数据库创建语句
    std::string sqlVersion; // 版本信息表创建语句

    std::map<std::string, std::map<int32_t, std::string>> sqlTables; // 创建表的语句
};

/**
 * @desc 具体服务类配置类
 * @auth Qiwei.Gu
 * @date 2015-04-15 14:39:17
 */
class DBCfg
{
public:
    bool init();

    const DBConfigInfo& getDBInfo() { return dbInfo_; }

private:
    DBConfigInfo dbInfo_;
};


#endif // __BASE_DB_CFG_H__
