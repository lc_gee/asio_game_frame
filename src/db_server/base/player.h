#ifndef __GAME_SERVER_BASE_PLAYER_H__
#define __GAME_SERVER_BASE_PLAYER_H__

#include <memory>

#include <tables.pb.h>

class Session;
union MessageID;

class Player
{
public:
    Player(uint32_t playerID);

    /**
     * @desc 初始化角色
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:03:24
     */
    bool init();

    /**
     * @desc 设置LoginGate连接
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:09:04
     */
    void setGsSession(const std::shared_ptr<Session>& session) { gsSession_ = session; }

    /**
     * @desc 查询LoginGate连接
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:10:56
     */
    const std::shared_ptr<Session>& getGsSession() { return gsSession_; }

    /**
     * @desc 向客户端发送消息, 内部实现LoginGate的转发封装
     * @param msgID 消息ID
     * @param msg 消息内容
     * @auth Qiwei.Gu
     * @date 2015-04-12 01:47:09
     */
    bool sendMsg(const MessageID& msgID, google::protobuf::Message& msg);

    /**
     * @desc 向对方发送消息
     * @param msgID 消息ID
     * @param msg 消息内容
     * @auth Qiwei.Gu
     * @date 2015-04-12 01:47:09
     */
    bool sendMsg(uint16_t moduleType, uint16_t msgID, google::protobuf::Message& msg);

    /**
     * @desc 查询角色ID
     * @auth Qiwei.Gu
     * @date 2015-05-04 15:00:10
     */
    uint32_t getID() { return playerID_; }

private:
    uint32_t playerID_;
    std::shared_ptr<Session> gsSession_;
};

#endif // __GAME_SERVER_BASE_PLAYER_H__
