#include "db_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <common/log/log.h>
#include <common/net/message_parse.h>
#include <common/net/session.h>

#include <protocols/protos/common.pb.h>
using namespace proto::common;
#include <protocols/protos/db.pb.h>
using namespace proto::db;
#include <protocols/protos/tables.pb.h>
using namespace proto::tables;

#include <common/utils/util.h>

#include "base/sub_server_mgr.h"

bool DbModule::doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                          const std::shared_ptr<Player>& player,
                          const MessageID& msgID,
                          const char* buf,
                          std::size_t bufSize)
{
    switch (msgID.stMsg.msgID)
    {
    case DB2S_COMMON_RET:
        return onCommonRet(submgr, player, msgID, buf, bufSize);

    default:
        return false;
    }

    return false;
}

bool DbModule::onCommonRet(const std::shared_ptr<SubServerMgr>& submgr,
                           const std::shared_ptr<Player>& player,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize)
{
    DbRet ret;
    if (!ret.ParseFromArray(buf, bufSize))
    {
        LOG_ERROR("Parse proto data failed.");
        return false;
    }

    LOG_DEBUG("EXECUTE DB SQL RESULT, error code: " << ret.err());
    return true;
}
