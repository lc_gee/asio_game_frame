#ifndef __MODULES_COMMON_COMMON_CMD_MODULE_H__
#define __MODULES_COMMON_COMMON_CMD_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/cmd_module.h>

class Session;

class CommonCmdModule : public ICmdModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                           const std::string& cmd,
                           const std::string& params);

private:
    bool onCommon(const std::shared_ptr<Session>& session,
                  const std::string& cmd,
                  const std::string& params);
};

#endif // __CMDMODULES_COMMON_COMMON_CMDMODULE_H__
