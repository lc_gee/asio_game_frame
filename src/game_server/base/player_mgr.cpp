#include "player_mgr.h"

#include <common/net/session.h>

#include "base/player.h"

bool PlayerMgr::init()
{
    return true;
}

void PlayerMgr::addPlayer(const std::shared_ptr<Player>& player)
{
    std::lock_guard<std::mutex> lock(mutex_);
    players_.insert(std::make_pair(player->getID(), player));
}
