#ifndef __GAME_SERVER_BASE_PLAYER_H__
#define __GAME_SERVER_BASE_PLAYER_H__

#include <memory>

#include <tables.pb.h>

class Session;
union MessageID;

class Player
{
public:
    Player(uint32_t playerID);

    /**
     * @desc 初始化角色
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:03:24
     */
    bool init();

    /**
     * @desc 是否在线
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:04:58
     */
    bool isOnline() { return isOnline_; }

    /**
     * @desc 设置LoginGate连接
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:09:04
     */
    void setLgSession(const std::shared_ptr<Session>& session) { lgSession_ = session; }

    /**
     * @desc 查询LoginGate连接
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:10:56
     */
    const std::shared_ptr<Session>& getLgSession() { return lgSession_; }

    /**
     * @desc 角色登录
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:13:31
     */
    bool onLogin();

    /**
     * @desc 角色登出
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:13:31
     */
    bool onLogout();

    /**
     * @desc 向客户端发送消息, 内部实现LoginGate的转发封装
     * @param msgID 消息ID
     * @param msg 消息内容
     * @auth Qiwei.Gu
     * @date 2015-04-12 01:47:09
     */
    bool sendMsg(const MessageID& msgID, google::protobuf::Message& msg);

    /**
     * @desc 向对方发送消息
     * @param msgID 消息ID
     * @param msg 消息内容
     * @auth Qiwei.Gu
     * @date 2015-04-12 01:47:09
     */
    bool sendMsg(uint16_t moduleType, uint16_t msgID, google::protobuf::Message& msg);

    /**
     * @desc 获得proto内部数据
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:44:15
     */
    const proto::tables::player& getProto() { return player_; }

    /**
     * @desc 获得角色ID
     * @auth Qiwei.Gu
     * @date 2015-04-30 15:44:37
     */
    uint32_t getID() { return player_.player_id(); }


private:
    proto::tables::player player_;
    bool isOnline_;
    std::shared_ptr<Session> lgSession_;
};

#endif // __GAME_SERVER_BASE_PLAYER_H__
