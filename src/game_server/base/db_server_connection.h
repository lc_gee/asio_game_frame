#ifndef __COMMON_NET_DB_CONNECTION_H__
#define __COMMON_NET_DB_CONNECTION_H__

#include <google/protobuf/message.h>

#include <common/net/session.h>

class DbServerConnection
{
public:
    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-04-24 00:01:23
     */
    bool init();

    /**
     * @desc 添加表记录
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-23 23:55:21
     */
    bool add(uint32_t playerID,
             google::protobuf::Message* table,
             uint16_t retModuleType = 1,
             uint16_t retMsgID = 2);

    /**
     * @desc 同时添加多个表， 如果有一个成功则回滚
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-24 02:47:36
     */
    bool add(uint32_t playerID,
             const std::vector<google::protobuf::Message*>& tables,
             uint16_t retModuleType = 1,
             uint16_t retMsgID = 2);

    /**
     * @desc 删除表记录
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-23 23:56:05
     */
    bool del(uint32_t playerID,
             google::protobuf::Message* table,
             uint16_t retModuleType = 1,
             uint16_t retMsgID = 2);

    /**
     * @desc 删除多个表记录
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-23 23:56:05
     */
    bool del(uint32_t playerID,
             const std::vector<google::protobuf::Message*>& tables,
             uint16_t retModuleType = 1,
             uint16_t retMsgID = 2);


    /**
     * @desc 更新表数据
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-23 23:54:32
     */
    bool update(uint32_t playerID,
                google::protobuf::Message* table,
                uint16_t retModuleType = 1,
                uint16_t retMsgID = 2);

    /**
     * @desc 更新多个表数据
     * @param table 需要保存的表对应的协议数据
     * @param retModuleType 处理SQL执行结果所对应的模块类型
     * @param retMsgID 执行结果所对应的消息ID
     * @auth Qiwei.Gu
     * @date 2015-04-23 23:54:32
     */
    bool update(uint32_t playerID,
                const std::vector<google::protobuf::Message*>& tables,
                uint16_t retModuleType = 1,
                uint16_t retMsgID = 2);

    std::shared_ptr<Session> getDbSession() { return dbSession_; }

private:
    bool dealTables(uint32_t playerID,
                    const std::vector<google::protobuf::Message*>& tables,
                    uint16_t reqMsgID,
                    uint16_t retModuleType, uint16_t retMsgID);



private:
    std::shared_ptr<Session> dbSession_;
};

#endif //  __COMMON_NET_DB_CONNECTION_H__
