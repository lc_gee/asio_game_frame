#include "sub_server_mgr.h"

#include <common/base/cmd_module.h>
#include <common/base/cmd_module_mgr.h>
#include <common/base/module_mgr.h>
#include <common/base/server_mgr.h>
#include <common/log/log.h>
#include <common/net/session.h>
#include <common/net/session_mgr.h>
#include <common/net/cmd_session.h>

#include <common.pb.h>

#include "modules/common/common_cmd_module.h"
#include "modules/login/login_cmd_module.h"
#include "modules/login/login_module.h"


SubServerMgr::SubServerMgr() : ISubServerMgr(ServerType::eClient)
{
}

bool SubServerMgr::init()
{
    if (!initModule())
    {
        LOG_ERROR("Init sub module manager failed.");
        return false;
    }
    if (!initCmdModule())
    {
        LOG_ERROR("Init sub cmd module manager failed.");
        return false;
    }
    LOG_INFO("Init sub session manager suc");
    return true;
}

std::shared_ptr<IManager> SubServerMgr::getSubMgr(SubManagerType type)
{
    // auto it = subMgrs_.find(type);
    // return it == subMgrs_.end() ? std::shared_ptr<IManager>() : it->second;
    return std::shared_ptr<IManager>();
}

/**
 * @desc 功能模块初始化
 * @auth Qiwei.Gu
 * @date 2015-04-05 22:04:58
 */
bool SubServerMgr::initModule()
{
    auto moduleMgr = ServerMgr::get().getMgr<ModuleMgr>(ManagerType::eModule);
    if (!moduleMgr)
    {
        LOG_ERROR("Module manager not inited.");
        return false;
    }
    if (!moduleMgr->addModule(proto::common::eLogin, std::make_shared<LoginModule>()))
    {
        LOG_ERROR("add login  module failed.");
        return false;
    }
    LOG_INFO("Init sub module manager suc");
    return true;
}

/**
 * @desc 命令模块出现化
 * @auth Qiwei.Gu
 * @date 2015-04-05 22:05:42
 */
bool SubServerMgr::initCmdModule()
{
    auto cmdModuleMgr = ServerMgr::get().getMgr<CmdModuleMgr>(ManagerType::eCmdModule);
    if (!cmdModuleMgr)
    {
        LOG_ERROR("Cmd Module manager not inited.");
        return false;
    }
    if (!cmdModuleMgr->addModule("common", std::make_shared<CommonCmdModule>()))
    {
        LOG_ERROR("Cmd Module manager add common module failed.");
        return false;
    }
    if (!cmdModuleMgr->addModule("login", std::make_shared<LoginCmdModule>()))
    {
        LOG_ERROR("Cmd Module manager add login module failed.");
        return false;
    }
    LOG_INFO("Init sub cmd module manager suc");
    return true;
}
