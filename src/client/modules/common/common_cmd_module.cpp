#include "common_cmd_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include "common/net/session.h"
#include "common/log/log.h"

bool CommonCmdModule::doDispatch(const std::shared_ptr<Session>& session,
                                const std::string& cmd,
                                const std::string& params)
{
    if (cmd == "common")
    {
        return onCommon(session, cmd, params);
    }
    return false;
}

bool CommonCmdModule::onCommon(const std::shared_ptr<Session>& session,
                               const std::string& cmd,
                               const std::string& params)
{
    LOG_DEBUG("CommonCmdModule::onCommon :" << cmd << " " << params );
    return true;
}
