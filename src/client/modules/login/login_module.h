#ifndef __MODULES_LOGIN_LOGIN_MODULE_H__
#define __MODULES_LOGIN_LOGIN_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/module.h>

class Session;

class LoginModule : public IModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                            const std::shared_ptr<Player>& player,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);


    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

private:
    bool onLoginRet(const std::shared_ptr<Session>& session,
                    const MessageID& msgID,
                    const char* buf,
                    std::size_t bufSize);

    bool onCreatePlayerRet(const std::shared_ptr<Session>& session,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize);

    bool onChoosePlayerRet(const std::shared_ptr<Session>& session,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize);

    bool onLoginConfirmRet(const std::shared_ptr<Session>& session,
                           const MessageID& msgID,
                           const char* buf,
                           std::size_t bufSize);

};

#endif // __MODULES_LOGIN_LOGIN_MODULE_H__
