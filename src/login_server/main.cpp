#include <iostream>

#include <log4cplus/loggingmacros.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>

using namespace log4cplus;
using namespace log4cplus::helpers;

#include <common/base/server_mgr.h>

#include "base/sub_server_mgr.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

int main()
{
    if (!ServerMgr::get().init(std::make_shared<SubServerMgr>()))
    {
        std::cerr << "Init Server manager failed." << std::endl;
        return 1;
    }
    return 0;
}
