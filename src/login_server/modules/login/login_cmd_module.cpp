#include "login_cmd_module.h"

#include <boost/asio.hpp>
using namespace boost;
using namespace boost::asio;

#include <common/net/session.h>

bool LoginCmdModule::doDispatch(const std::shared_ptr<Session>& session,
                               const std::string& cmd,
                               const std::string& params)
{
    if (cmd == "login")
    {
        return onLogin(session, cmd, params);
    }
    return false;
}

bool LoginCmdModule::onLogin(const std::shared_ptr<Session>& session,
                             const std::string& cmd,
                             const std::string& params)
{
    return true;
}
