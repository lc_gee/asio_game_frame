#ifndef __MODULES_LOGIN_LOGIN_MODULE_H__
#define __MODULES_LOGIN_LOGIN_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/module.h>

class Session;

class LoginModule : public IModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

private:
    bool onLogin(const std::shared_ptr<Session>& session,
                 const MessageID& msgID,
                 const char* buf,
                 std::size_t bufSize);
};

#endif // __MODULES_LOGIN_LOGIN_MODULE_H__
