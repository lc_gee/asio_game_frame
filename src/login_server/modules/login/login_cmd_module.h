#ifndef __MODULES_LOGIN_LOGIN_CMD_MODULE_H__
#define __MODULES_LOGIN_LOGIN_CMD_MODULE_H__

#include <boost/asio.hpp>

#include <common/base/cmd_module.h>

class Session;

class LoginCmdModule : public ICmdModule
{
public:
    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                           const std::string& cmd,
                           const std::string& params);

private:
    bool onLogin(const std::shared_ptr<Session>& session,
                 const std::string& cmd,
                 const std::string& params);
};

#endif // __CMDMODULES_LOGIN_LOGIN_CMDMODULE_H__
