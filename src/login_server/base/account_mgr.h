#ifndef __BASE_ACCOUNT_MGR_H__
#define __BASE_ACCOUNT_MGR_H__

#include <proto/login/login.pb.h>

#include <common/base/manager.h>

struct AccountInfo
{
    std::string accountID;
    std::string source;
    int32_t lastArea;
    time_t lastLoginTime;
    std::string lastIP;
    proto::login::AccountAreaHistorys areaHistory;
};

class AccountMgr : public IManager
{
public:
    ~AccountMgr() {}

    virtual bool init();

    std::shared_ptr<AccountInfo> getAccount(std::string accoundID)
    {
        auto it = accounts_.find(accoundID);
        return it == accounts_.end() ? std::shared_ptr<AccountInfo>() : it->second;
    }

private:
    std::map<std::string, std::shared_ptr<AccountInfo> > accounts_;
};

#endif //  __BASE_ACCOUNT_MGR_H__
