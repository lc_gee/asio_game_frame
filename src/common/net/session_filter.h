#ifndef __COMMON_NET_SESSION_FILTER_H__
#define __COMMON_NET_SESSION_FILTER_H__

class Session;

#include <boost/asio.hpp>

enum FilterResult : uint16_t
{
    eFilterFailed,       // 过滤失败，直接返回
    eFilterSucContinue,     // 过滤成功, 继续下一个
    eFilterSucSkipOthers,   // 过滤成功, 但放弃其他过滤器
    eFilterSucReturn,   // 过虑成功，但直接返回
};

class ISessionFilter
{
public:
    virtual ~ISessionFilter() {}

    virtual FilterResult handleReadFilter(const std::shared_ptr<Session>& session,
                                          const boost::system::error_code& e,
                                          std::size_t bytes_transferred)
    {
        return eFilterSucContinue;
    }

};

#endif //  __COMMON_NET_SESSION_FILTER_H__
