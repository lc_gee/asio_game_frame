#include "log.h"

#include <log4cplus/loggingmacros.h>
#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/stringhelper.h>

using namespace log4cplus;
using namespace log4cplus::helpers;

#include "common/base/config.h"
#include "common/base/config_mgr.h"
#include "common/base/server_mgr.h"


Logger g_rootLogger;

bool initLog(const std::string& logCfgFile)
{
    log4cplus::initialize();
    return loadLogCfgFile(logCfgFile);
}

bool loadLogCfgFile(const std::string& logCfgFile)
{
    PropertyConfigurator::doConfigure(logCfgFile);

    g_rootLogger = Logger::getRoot();
    // g_loginLogger = Logger::getInstance("login");

    LOG4CPLUS_DEBUG(g_rootLogger, "log init suc.");
    return true;
}
