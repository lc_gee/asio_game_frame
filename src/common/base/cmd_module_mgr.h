#ifndef __CMD_MODULE_MGR_H__
#define __CMD_MODULE_MGR_H__

#include <map>
#include <memory>

#include "manager.h"

class ICmdModule;

class CmdModuleMgr : public IManager, public std::enable_shared_from_this<CmdModuleMgr>
{
public:
    virtual ~CmdModuleMgr() {}

    bool init();

    bool addModule(const std::string& moduleName, std::shared_ptr<ICmdModule> module);

    std::shared_ptr<ICmdModule> getModule(const std::string& moduleName);

    template<typename T>
    std::shared_ptr<T> getModule(const std::string& moduleName)
    {
        auto module = getModule(moduleName);
        return module ? std::dynamic_pointer_cast<T>(module) : std::shared_ptr<T>();
    }

private:
    std::map<std::string, std::shared_ptr<ICmdModule>> modules_;
};

#endif // __CMD_MODULE_MGR_H__
