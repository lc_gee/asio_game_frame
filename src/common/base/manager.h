#ifndef __COMMON_MANAGER_H__
#define __COMMON_MANAGER_H__

#include <memory>

class IManager
{
public:
	virtual ~IManager() {}

	virtual bool init() = 0;
};

#endif // __COMMON_MANAGER_H__



