#include "module.h"
#include "log/log.h"

bool IModule::doTranslate(const std::shared_ptr<Session>& session,
                         const MessageID& msgID,
                         const char* buf,
                         std::size_t bufSize)
{
    LOG_WARN("doTranslate not implement.");
    return false;
}

bool IModule::doDispatch(const std::shared_ptr<Session>& session,
                        const MessageID& msgID,
                        const char* buf,
                        std::size_t bufSize)
{
    LOG_WARN("doDispatch not implement.");
    return false;
}


// bool IModule::doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
//                         const std::shared_ptr<Player>& player,
//                         const MessageID& msgID,
//                         const char* buf,
//                         std::size_t bufSize)
// {
//     LOG_WARN("doDispatch not implement.");
//     return false;
// }
