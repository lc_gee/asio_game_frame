#ifndef __COMMON_CMD_MODULE_H__
#define __COMMON_CMD_MODULE_H__

#include <memory>

#include <boost/asio.hpp>

class Session;

class ICmdModule
{
public:
    virtual ~ICmdModule() {}

    /**
     * @desc 命令转发器
     * @param session 连接会话
     * @param fullCmd 命令，格式： moduleName@cmdName param1 param2 ...
     * @auth Qiwei.Gu
     * @date 2015-04-06 00:14:35
     */
    static bool doDispatch(const std::shared_ptr<Session>& session,
                            const std::string& fullCmd);

    /**
     * @desc 命令转发器
     * @param session 连接会话
     * @param cmd 指令名
     * @param params 指令参数
     * @auth Qiwei.Gu
     * @date 2015-04-06 00:14:35
     */
    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                            const std::string& cmd,
                            const std::string& params) = 0;
};

#endif // __COMMON_CMD_MODULE_H__
