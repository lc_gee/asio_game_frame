#ifndef __COMMON_MODULE_H__
#define __COMMON_MODULE_H__

#include <memory>

#include <boost/asio.hpp>

class Player;
class Session;
class SubServerMgr;

union MessageID;

class IModule
{
public:
    virtual ~IModule() {}

    /**
     * @desc 用于向下转发消息
     * @auth Qiwei.Gu
     * @date 2015-04-30 21:12:11
     */
    virtual bool doTranslate(const std::shared_ptr<Session>& session,
                             const MessageID& msgID,
                             const char* buf,
                             std::size_t bufSize);

    /**
     * @desc 正常消息
     * @auth Qiwei.Gu
     * @date 2015-04-30 21:12:33
     */
    virtual bool doDispatch(const std::shared_ptr<Session>& session,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize);

    /**
     * @desc 向上转发消息
     * @auth Qiwei.Gu
     * @date 2015-04-30 21:12:46
     */
    virtual bool doDispatch(const std::shared_ptr<SubServerMgr>& submgr,
                            const std::shared_ptr<Player>& player,
                            const MessageID& msgID,
                            const char* buf,
                            std::size_t bufSize) = 0;
};

#endif // __COMMON_MODULE_H__
