#ifndef __COMMON_BASE_CONFIG_MGR_H__
#define __COMMON_BASE_CONFIG_MGR_H__

#include <map>
#include <memory>

#include "manager.h"

class IConfig;

enum ConfigType : uint16_t;

class ConfigMgr : public IManager, public std::enable_shared_from_this<ConfigMgr>
{
public:
    virtual ~ConfigMgr() {}

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:48:37
     */
    bool init();

    /**
     * @desc 重新加载指定配置
     * @param type 配置类型
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:49:06
     */
    bool reload(ConfigType type);

    /**
     * @desc 添加新的配置，并初始化
     * @param type 配置类型
     * @param config 配置
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:49:41
     */
    bool addConfig(ConfigType type, std::shared_ptr<IConfig> config);

    /**
     * @desc 添加新的配置
     * @param type 配置类型
     * @param config 配置
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:49:41
     */
    bool addConfigNoInit(ConfigType type, std::shared_ptr<IConfig> config);

    /**
     * @desc 查询配置
     * @param type 配置类型
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:50:16
     */
    std::shared_ptr<IConfig> getConfig(ConfigType type);

    /**
     * @desc 查询配置，并转换为相应的配置类型
     * @param type 配置类型
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:50:16
     */
    template<typename T>
    std::shared_ptr<T> getConfig(ConfigType type)
    {
        auto config = getConfig(type);
        return config ? std::dynamic_pointer_cast<T>(config) : std::shared_ptr<T>();
    }

private:
    std::map<ConfigType, std::shared_ptr<IConfig>> configs_;
};

#endif // __COMMON_BASECONFIG_MGR_H__
