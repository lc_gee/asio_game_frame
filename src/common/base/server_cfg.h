#ifndef __COMMON_SERVER_CFG_H__
#define __COMMON_SERVER_CFG_H__

#include <list>
#include <map>
#include <memory>
#include <vector>

#include "config.h"

#include <pugixml.hpp>

class CfgFileInfo;

enum ServerType : uint16_t;
enum SessionType : uint16_t;

/**
 * 服务器网络配置信息
 */
struct ServerNetInfo
{
    ServerType serverType; ///< 服务器类型
    uint16_t serverID;
    SessionType sessionType;
    std::string name;      ///< 服务器名称
    std::string addr;      ///< 网络地址
    std::string port;      ///< 网络端口
    uint32_t reconnInterval; ///< 重连时间间隔，单位毫秒
    bool autoInit;

    ServerNetInfo();

    uint64_t getID();

    bool parseXml(const pugi::xml_node& node);
};

/**
 * 服务器配置文件信息
 */
struct CfgFileInfo
{
    ServerType serverType;   ///< 服务器类型
    std::string serverName;  ///< 服务器名称
    std::string cfgFile;     ///< 配置文件名

    CfgFileInfo();
};

/**
 * @desc 服务器配置类
 * @auth Qiwei.Gu
 * @date 2015-03-25 21:22:10
 */
class ServerCfg : public IConfig
{
public:
    /**
     * @desc 构造函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 21:46:54
     */
    ServerCfg();

    /**
     * @desc 析构函数
     * @auth Qiwei.Gu
     * @date 2015-03-25 21:23:07
     */
    virtual ~ServerCfg() {}

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-03-25 21:23:19
     */
    virtual bool init();

    /**
     * @desc 初始化
     * @auth Qiwei.Gu
     * @date 2015-03-25 21:23:19
     */
    virtual bool init(ServerType type);

    /**
     * @desc 创建新的实例
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:22:35
     */
    virtual std::shared_ptr<IConfig> makeNewObject();

    /**
     * @desc 循环遍历监听配置并执行回调函数
     * @auth Qiwei.Gu
     * @date 2015-03-26 22:14:53
     */
    bool loopListens(std::function<bool (const std::shared_ptr<ServerNetInfo>&)> func);

    /**
     * @desc 循环遍历连接配置并执行回调函数
     * @auth Qiwei.Gu
     * @date 2015-03-26 22:19:06
     */
    bool loopConns(std::function<bool (const std::shared_ptr<ServerNetInfo>&)> func);

    /**
     * @desc 获得日志配置文件路径
     * @auth Qiwei.Gu
     * @date 2015-03-25 22:41:20
     */
    const std::string& getLogCfgFile() { return logCfgFile_; }

    /**
     * @desc 查询连接配置
     * @auth Qiwei.Gu
     * @date 2015-04-21 18:33:17
     */
    void getConnNetInfos(ServerType serverType, SessionType sessionType,
                         std::vector<std::shared_ptr<ServerNetInfo>>& netInfos);

    /**
     * @desc 服务器配置信息
     * @auth Qiwei.Gu
     * @date 2015-04-28 20:11:57
     */
    CfgFileInfo& getCfgFileInfo() { return cfgFile_; }

private:
    std::string logCfgFile_;                                              // 日志配置文件
    // std::string subMgrCfgFile_;                                           // 具体服务器配置文件
    CfgFileInfo cfgFile_;                                                 // 各个服务器配置文件信息
    std::map<ServerType, std::shared_ptr<ServerNetInfo> > netListens_;    // 本服务器监听配置
    std::map<std::pair<ServerType, SessionType>, std::map<uint16_t, std::shared_ptr<ServerNetInfo> > > netConns_; // 服务器连接配置
};

#endif // __COMMON_SERVER_CFG_H__
