#ifndef __COMMON_BASE_CONFIG_H__
#define __COMMON_BASE_CONFIG_H__

#include <memory>

class IConfig
{
public:
    virtual ~IConfig() {}

    /**
     * @desc 初始化配置
     * @return 成功与否
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:21:08
     */
    virtual bool init() = 0;

    /**
     * @desc 创建新的实例
     * @auth Qiwei.Gu
     * @date 2015-04-06 16:22:35
     */
    virtual std::shared_ptr<IConfig> makeNewObject() = 0;

};

#endif // __COMMON_BASE_CONFIG_H__
